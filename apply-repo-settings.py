#!/usr/bin/env python3

import gitlab
import time

NON_PKG_PROJECTS = [
                    "docker-images",
                    "issues",
                    "mobian-recipes",
                    "mobian-server",
                    "packaging-tools",
                  ]

DEFAULT_CI_PIPELINE = "mobian-ci.yml@Mobian-team/packaging-tools"
DEFAULT_BRANCH = "mobian"

gl = gitlab.Gitlab.from_config('salsa')

groups = gl.groups.list(search="Mobian-team")

PROTECTED_BRANCH_DATA = {
    "name": "mobian*",
    "push_access_level": gitlab.const.MAINTAINER_ACCESS,
    "merge_access_level": gitlab.const.DEVELOPER_ACCESS,
    "code_owner_approval_required": False
}

PROTECTED_TAG_DATA = {
    "name": "mobian*",
    "create_access_level": gitlab.const.MAINTAINER_ACCESS,
}

for grp in groups:
    group = gl.groups.get(grp.id)
    for prj in group.projects.list(all=True, archived=False):
        print("Processing project", prj.name)
        project = gl.projects.get(prj.id)
        if group.name != "devices" or not "linux" in project.name:
            try:
                branch = project.branches.get(DEFAULT_BRANCH)
                project.default_branch = DEFAULT_BRANCH
            except gitlab.GitlabGetError as e:
                print("No branch named", DEFAULT_BRANCH, "in this project")
        project.merge_method = "ff"
        project.save()

        if project.name in NON_PKG_PROJECTS:
            continue

        try:
            protected_branch = project.protectedbranches.get("mobian*")
        except gitlab.GitlabGetError as e:
            print("\tProtecting branches mobian*")
            project.protectedbranches.create(PROTECTED_BRANCH_DATA)

        try:
            protected_tag = project.protectedtags.get("mobian*")
        except Exception as e:
            print("\tProtecting tags mobian*")
            project.protectedtags.create(PROTECTED_TAG_DATA)

        project.ci_config_path = DEFAULT_CI_PIPELINE
        project.save()
